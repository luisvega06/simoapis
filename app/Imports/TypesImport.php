<?php

namespace App\Imports;

use App\Type;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;

class TypesImport implements ToModel
{
    /** 
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $name="No encontré";
        
        if($row[52] == "SI"){
            $name = "GO BLUE";
        }elseif($row[73] == "NO" && $row[85] == "NO" && $row[52] != "SI"){
            $name = "VOD"; 
        }elseif($row[73] == "SI" && $row[85] == "NO"){
            $name = "PDA";
        }elseif($row[85] == "SI" && $row[73] == "NO"){
            $name = "PDV";
        }elseif($row[85] == "SI" && $row[73] == "SI"){
            $name = "MIXTO";
        }
        try{
            Type::where('name', $name)->first()->id;
 
        }catch(Exception $e){
            return new Type([
                'name' => $name,
            ]);
        }
    }
}
