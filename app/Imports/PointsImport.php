<?php

namespace App\Imports;

use App\Circuit;
use App\Point;
use App\Type;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PointsImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }
    public function model(array $row)
    {
        $id_de_el_pop = null;
        if($row[52] == "SI"){
            $type = "GO BLUE";
            try{
                $id_de_el_pop = Type::where('name', "GO BLUE")->first()->id;
            }catch(Exception $e){
                $id_de_el_pop = null;
            }

        }elseif($row[73] == "NO" && $row[85] == "NO"){
            $type = "VOD";
            try{
                $id_de_el_pop = Type::where('name', "VOD")->first()->id;
            }catch(Exception $e){
                $id_de_el_pop = null;
            }
        }elseif($row[73] == "SI" && $row[85] == "NO"){
            $type = "PDA";
            try{
                $id_de_el_pop = Type::where('name', "PDA")->first()->id;
            }catch(Exception $e){
                $id_de_el_pop = null;
            }

        }elseif($row[85] == "SI" && $row[73] == "NO"){
            $type = "PDV";
            try{
                $id_de_el_pop = Type::where('name', "PDV")->first()->id;
            }catch(Exception $e){
                $id_de_el_pop = null;
            }

        }elseif($row[85] == "SI" && $row[73] == "SI"){
            $type = "MIXTO";
            try{
                $id_de_el_pop = Type::where('name', "MIXTO")->first()->id;
            }catch(Exception $e){
                $id_de_el_pop = null;
            }

        }

        
        

        try{
            $id_de_el_circuito = Circuit::where('name', $row[5])->first()->id;
        }catch(Exception $e){
            $id_de_el_circuito = null;
        }



        $point=Point::where('code', '=', $row[0])->get();
        //dd(count($point));
        if(count($point)==0){
            return new point([
                'code'              => $row[0],
                'name'              => $row[1],
                'status'            => $row[2],

                
                'channel'           => $row[3],
                'type_id'           => $id_de_el_pop,
                

                'phone'             => $row[8],
                'cell_phone'        => $row[9],
                'owner'             => $row[10],
                'document_type'     => $row[11],
                'document'          => $row[12],
                'schedule'          => $row[13],
                'department'        => $row[15],
                'city'              => $row[16],
                'neighborhood'      => $row[17],
                'address'           => $row[18],
                
                'latitude'          => str_replace(".",",",substr($row[19], 0, -1)),
                'longitude'         => str_replace(".",",",substr($row[20], 0, -1)),
                'status_dms'        => $row[25],
                'creation'          => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[31]),
                'last_modification' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[32]),
                'code_sub'          => $row[43],
                'simcard_service'   => $row[73],
                'mbox_service'      => $row[85],

                'circuit_id'        => $id_de_el_circuito,
            ]);  
        }else{
            /*return $point->update([
                
                    'name'              => $row[1],
                    'status'            => $row[2],

                    
                    'channel'           => $row[3],
                    'type_id'           => $id_de_el_pop,
                    

                    'phone'             => $row[8],
                    'cell_phone'        => $row[9],
                    'owner'             => $row[10],
                    'document_type'     => $row[11],
                    'document'          => $row[12],
                    'schedule'          => $row[13],
                    'department'        => $row[15],
                    'city'              => $row[16],
                    'neighborhood'      => $row[17],
                    'address'           => $row[18],
                    
                    'latitude'          => str_replace(".",",",substr($row[19], 0, -1)),
                    'longitude'         => str_replace(".",",",substr($row[20], 0, -1)),
                    'status_dms'        => $row[25],
                    'creation'          => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[31]),
                    'last_modification' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[32]),
                    'code_sub'          => $row[43],

                    'circuit_id'        => $id_de_el_circuito,
                ]);*/
        }
    }
}
