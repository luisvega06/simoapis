<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class SupervisorsImport implements ToModel
{ 
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int 
    {
        return 2;
    }
    public function model(array $row)
    { 

        $user=User::where('identification_card', '=', $row[8])->get();
        if(count($user)==0){
            return new User([ 
                'name'                => $row[9],
                'identification_card' => $row[8],
                'password'            => bcrypt($row[8]),
                'cellphone'           => "777",
                'email'               => $row[9] . "@" . "gmail.com",
                'canal_id'            => 1,
                'dealer_id'           => 1,
                'zone_id'             => 1,
                'position_id'         => 2,
            ]);
        }
    }
}
