<?php

namespace App\Imports;

use App\Adviser;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class AdvisersPDVImport implements ToModel
{
    /**
    * @param array $row 
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 2;
    }

    public function model(array $row)
    {
        $id_supervisor = User::where('identification_card', '=', $row[8])->first()->id;
        $adviser=Adviser::where('identification_card', '=', $row[3])->get();
        if(count($adviser)==0){
            return new Adviser([
                'name'                => $row[4],
                'identification_card' => $row[3],
                'password'            => bcrypt($row[3]),
                'cellphone'           => "777",
                'type_adviser_id'     => 2,
                'canal_id'            => 1,
                'user_id'             => $id_supervisor, 
                'zone_id'             => 1,
            ]);
        }
    }
}
