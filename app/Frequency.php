<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    protected $fillable = [
        'name',
        'monday',
        'tuesday',
        'wenesday',
        'thursday',
        'friday',
        'saturday',
        'sunday'
    ]; 
    public function circuits()
    {
        return $this->hasMany('App\Circuit');
    }
}
