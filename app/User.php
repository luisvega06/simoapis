<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        "name",
        "identification_card",
        "password",
        "cellphone",
        "email", 
        "canal_id",
        "dealer_id",
        "zone_id",
        "position_id"
    ];
                               
    
    public function canal()
    {
        return $this->belongsTo('App\Canal');
    }

    public function dealer()
    {
        return $this->belongsTo('App\Dealer');
    }

    public function position()
    { 
        return $this->belongsTo('App\Position');
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }
}
