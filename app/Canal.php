<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canal extends Model
{ 
    protected $fillable = [
        'name'
    ];
    public function users()
    { 
        return $this->hasMany('App\User');
    }

    public function advisers()
    {
        return $this->hasMany('App\Advisers');
    }
}
