<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Adviser extends Model 
{
    protected $fillable = [
        'name',
        'identification_card',
        'cell_phone',
        'password',
        'type_adviser_id',
        'canal_id', 
        'zone_id',
        'user_id'
    ];
    public function canal() 
    {
        return $this->belongsTo('App\Canal');
    }

    public function circuits()
    {
        return $this->belongsToMany(Circuit::class);
    }

    public function dealer()
    {
        return $this->belongsTo('App\Dealer');
    }

    public function typeAdviser()
    {
        return $this->belongsTo('App\TypeAdviser');
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }
}
