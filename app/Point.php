<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
        'code',
        'name',
        'status',
        'channel',
        'type_id',
        'phone',
        'cell_phone',
        'owner',
        'document_type',
        'document',
        'category',
        'schedule',
        'department',
        'city',
        'neighborhood', 
        'address',
        'status_dms',
        'code_sub',
        'creation',
        'last_modification',
        'latitude',
        'longitude',
        'simcard_service',
        'mbox_service',
        'circuit_id',
    ];
    public function circuit()
    {
        return $this->belongsTo('App\Circuit');
    }
    public function typePoint(){
        return $this->belongsTo(TypePoint::class);
    }
}
