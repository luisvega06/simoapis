<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAdviser extends Model
{
    protected $fillable = [
        'name'
    ];
    public function advisers()
    {
        return $this->hasMany('App\Adviser');
    }
}
