<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Circuit extends Model
{
    protected $fillable = [
        'name', 
        'frequency_id'
    ];
    public function frequency()
    {
        return $this->belongsTo('App\Frequency');
    }

    public function advisers()
    {
        return $this->belongsToMany(Adviser::class);
    }

    public function points()
    {
        return $this->hasMany('App\Point');
    }
}
