<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    protected $fillable = [
        'name',
        'department_id'
    ];
    public function users()
    { 
        return $this->hasMany('App\User');
    }

    public function advisers()
    {
        return $this->hasMany('App\Adviser');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
