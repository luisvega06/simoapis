<?php

namespace App\Http\Controllers;

use App\Canal;
use App\Canals;
use Illuminate\Http\Request;

class CanalController extends Controller
{
   
    public function index()
    {
        $canal = Canal::all();
        return response()->json(["canal"=>$canal]);
    }

    
    public function store(Request $request)
    {
        $canal = Canal::create($request->input());
        return response()->json(["canal"=>$canal]);
    }

    
    public function show($id)
    {
        $canal = Canal::find($id);
        return response()->json(["canal"=>$canal]);
    }

    
    public function update(Request $request, $id)
    {
        $canal = Canal::find($id);
        $canal->update($request->input());
        return response()->json(["canal"=>$canal]);
    }


    public function destroy($id)
    {
        try {
            $canal = Canal::findOrFail($id);
            $canal->delete();
            return response()->json(["msj"=>"el canal fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el canal que quiere eliminar"]);   
        }
    }
}
