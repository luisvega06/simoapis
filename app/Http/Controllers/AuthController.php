<?php

namespace App\Http\Controllers;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use App\Adviser;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected function jwt(Adviser $user)
    {
    	$datos = [
    	'name' 			=>	$user->nombre,
    	'identification_card' 	=>	$user->identification_card,
    	'cellphone' 		=>	$user->cellphone,
    	'id'			=>  $user->id,
    	'iat' 			=> time(),
        'exp' 			=> time() + 60*60
    	];

    	return JWT::encode($datos, env('JWT_SECRET'));
    }

    public function login(Request $request)
    {
    	$user = User::where('identification_card', $request->input('identification_card'))->first();
    	if(!$user)	return response()->json(["status"=>400, "data"=>"El identification_card no existe"],404);
    	

    	if(Hash::check($request->input('password'),$user->password))
    	{
    		return response()->json(["status"=>200, "token"=> $this->jwt($user)]);
    	}
		
    	return response()->json(["status"=> 400, "data" => "Credenciales incorrectas"],400);

    	

    }
}
