<?php

namespace App\Http\Controllers;

use App\Zone;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    public function index()
    { 
        $zone = Zone::all();
        return response()->json(["zone"=>$zone]);
    }

    
    public function store(Request $request)
    {
        $zone = Zone::create($request->input());
        return response()->json(["zone"=>$zone]);
    }

    
    public function show($id)
    {
        $zone = Zone::find($id);
        return response()->json(["zone"=>$zone]);
    }

    
    public function update(Request $request, $id)
    {
        $zone = Zone::find($id);
        $zone->update($request->input());
        return response()->json(["zone"=>$zone]);
    }


    public function destroy($id)
    {
        try {
            $zone = Zone::findOrFail($id);
            $zone->delete();
            return response()->json(["msj"=>"la zona fue eliminada con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe la zona que quiere eliminar"]);   
        }
    }
}
