<?php

namespace App\Http\Controllers;

use App\Circuit;
use Illuminate\Http\Request;

class CircuitController extends Controller
{
    public function index()
    {
        $circuit = Circuit::all();
        return response()->json(["circuit"=>$circuit]);
    }

    
    public function store(Request $request)
    {
        $circuit = Circuit::create($request->input());
        return response()->json(["circuit"=>$circuit]);
    }

    
    public function show($id)
    {
        $circuit = Circuit::find($id);
        return response()->json(["circuit"=>$circuit]);
    }

    
    public function update(Request $request, $id)
    {
        $circuit = Circuit::find($id);
        $circuit->update($request->input());
        return response()->json(["circuit"=>$circuit]);
    }


    public function destroy($id)
    {
        try {
            $circuit = Circuit::findOrFail($id);
            $circuit->delete();
            return response()->json(["msj"=>"el circuito fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el circuito que quiere eliminar"]);   
        }
    }
}
