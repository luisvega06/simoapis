<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request; 

class PositionController extends Controller
{
    
    public function index()
    {
        $position = Position::all();
        return response()->json(["position"=>$position]);
    }

    
    public function store(Request $request)
    {
        $position = Position::create($request->input());
        return response()->json(["position"=>$position]);
    }

    
    public function show($id)
    {
        $position = Position::find($id);
        return response()->json(["position"=>$position]);
    }

    
    public function update(Request $request, $id)
    {
        $position = Position::find($id);
        $position->update($request->input());
        return response()->json(["position"=>$position]);
    }


    public function destroy($id)
    {
        try {
            $position = Position::findOrFail($id);
            $position->delete();
            return response()->json(["msj"=>"la posición fue eliminada con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe la posición que quiere eliminar"]);   
        }
    }

}
