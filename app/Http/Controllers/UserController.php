<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidationUser;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct() 
    { 
        $this->middleware('jwt.auth')->except(["store"]);
    }
    
    
    public function index()
    {
        $users = User::all();
        return response()->json(["users"=>$users]);
    }

    
    public function store(ValidationUser $request)
    {
        $user = User::create($request->input());
       
        return response()->json(["user"=>$user]);
    }
    

    public function show($id)
    {
        $user = User::find($id);
        return response()->json(["user"=>$user]);
    }
 
    
    public function update(ValidationUser $request, $id)
    {
        $user = User::find($id);
        $user->update($request->input());
        return response()->json(["user"=>$user]);
    }

 
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(["msj"=>"el usuario fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el usuario que quiere eliminar"]);   
        }
    }

}
