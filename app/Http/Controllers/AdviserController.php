<?php

namespace App\Http\Controllers;

use App\Adviser;
use Illuminate\Http\Request;

class AdvisersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advisers = Adviser::all();
        return response()->json(["advisers"=>$advisers]);
    }

   
    public function store(Request $request)
    {
        $adviser = Adviser::create($request->input());
        $adviser->circuits()->attach($request->circuits);

        return response()->json(["adviser"=>$adviser]);
    }

    
    public function show($id)
    {
        $advisers = Adviser::find($id);
        return response()->json(["advisers"=>$advisers]);
    }

    public function update(Request $request, $id)
    {
        $adviser = Adviser::find($id);
        $adviser->circuits()->sync($request->circuits);
        return response()->json(["adviser"=>$adviser]);
    }

    public function destroy($id)
    {
        try {
            $adviser = Adviser::findOrFail($id);
            $adviser->circuits()->detach();
            $adviser->delete();
            return response()->json(["msj"=>"el asesor fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el asesor que quiere eliminar"]);   
        }
    }
}
