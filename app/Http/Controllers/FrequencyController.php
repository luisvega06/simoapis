<?php

namespace App\Http\Controllers;

use App\Frequency;
use Illuminate\Http\Request;

class FrequencyController extends Controller
{
    public function index()
    {
        $frequency = Frequency::all();
        return response()->json(["frequency"=>$frequency]);
    }

    
    public function store(Request $request)
    {
        $frequency = Frequency::create($request->input());
        return response()->json(["frequency"=>$frequency]);
    }

    
    public function show($id)
    {
        $frequency = Frequency::find($id);
        return response()->json(["frequency"=>$frequency]);
    }

    
    public function update(Request $request, $id)
    {
        $frequency = Frequency::find($id);
        return response()->json(["frequency"=>$frequency]);
    }


    public function destroy($id)
    {
        try {
            $frequency = Frequency::findOrFail($id);
            $frequency->delete();
            return response()->json(["msj"=>"el frequencia fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el frequencia que quiere eliminar"]);   
        }
    }
}
