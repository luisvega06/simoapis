<?php

namespace App\Http\Controllers;

use App\Imports\AdvisersImport;
use App\Imports\AdvisersPDAImport;
use App\Imports\AdvisersPDVImport;
use App\Point;
use App\Imports\CircuitsImport; 
use App\Imports\PointsImport;
use App\Imports\SupervisorsImport;
use App\Imports\TypesImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PointController extends Controller
{
    public function index()
    {
        $point = Point::all();
        return response()->json(["point"=>$point]);
    }

    
    public function store(Request $request)
    {
        $point = Point::create($request->input());
        return response()->json(["point"=>$point]);
    }

    
    public function show($id)
    {
        $point = Point::find($id);
        return response()->json(["point"=>$point]);
    }

    
    public function update(Request $request, $id)
    {
        $point = Point::find($id);
        $point->update($request->input());
        return response()->json(["point"=>$point]);
    }


    public function destroy($id)
    {
        try {
            $point = Point::findOrFail($id);
            $point->delete();
            return response()->json(["msj"=>"el cierucito fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el cierucito que quiere eliminar"]);   
        }
    }

    

    public function chargeFile(){
        return view('upload-file.index');
    } 
    public function importPointsExcel(Request $request){
        $file = $request->file('filePoint');
        if(!$file){
            return back();
        }
        Excel::import(new CircuitsImport, $file);
        Excel::import(new TypesImport, $file);
        Excel::import(new PointsImport, $file);
        /*$points = Excel::toCollection(new PointsImport(), $file);
        //dd($points[0]);
        foreach($points[0] as $point){
            Point::where("code", $point[0])->update([
                "name" => $point[1]
            ]);
        }*/
        return back()->with('message', 'Importacion de r point completada ');
    }
    public function importPointsEditExcel(Request $request){
        $file = $request->file('filePoint');
        if(!$file){
            return back();
        }
        $points = Excel::toCollection(new PointsImport(), $file);
        //dd($points[0]);
        
        foreach($points[0] as $point){
            if(Point::where("code", $point[0])){
                Point::where("code", $point[0])->update([
                    "name"              => $point[1],
                    'status'            => $point[2],
                    'channel'           => $point[3],
                    //'type_id'           => $id_de_el_pop,
                    'phone'             => $point[8],
                    'cell_phone'        => $point[9],
                    'owner'             => $point[10],
                    'document_type'     => $point[11],
                    'document'          => $point[12],
                    'schedule'          => $point[13],
                    'department'        => $point[15],
                    'city'              => $point[16],
                    'neighborhood'      => $point[17],
                    'address'           => $point[18],
                    
                    'latitude'          => str_replace(".",",",substr($point[19], 0, -1)),
                    'longitude'         => str_replace(".",",",substr($point[20], 0, -1)),
                    'status_dms'        => $point[25],
                    'creation'          => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($point[31]),
                    'last_modification' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($point[32]),
                    'code_sub'          => $point[43],

                    'simcard_service'   => $point[73],
                    'mbox_service'      => $point[85],

                    //'circuit_id'        => $id_de_el_circuito,
                ]);
            }
            /*Point::where("code", $point[0])->update([
                "name" => $point[1]
            ]);*/
        }
        return back()->with('message', 'Actualización de r point completada ');
    }



    public function importStructureDealerEditExcel(Request $request){
        $file = $request->file('fileStructureDealer');
        if(!$file){
            return back();
        }
        Excel::import(new SupervisorsImport, $file);
        Excel::import(new AdvisersPDAImport, $file);
        Excel::import(new AdvisersPDVImport, $file);
        return back()->with('message', 'Importacion de estructura dealer completada ');

    }
}
