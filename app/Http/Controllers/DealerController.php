<?php

namespace App\Http\Controllers;

use App\Dealer;
use Illuminate\Http\Request;

class DealerController extends Controller
{
    public function index()
    {
        $dealer = Dealer::all();
        return response()->json(["dealer"=>$dealer]);
    }

    
    public function store(Request $request)
    {
        $dealer = Dealer::create($request->input());
        return response()->json(["dealer"=>$dealer]);
    }

    
    public function show($id)
    {
        $dealer = Dealer::find($id);
        return response()->json(["dealer"=>$dealer]);
    }

    
    public function update(Request $request, $id)
    {
        $dealer = Dealer::find($id);
        $dealer->update($request->input());
        return response()->json(["dealer"=>$dealer]);
    }


    public function destroy($id)
    {
        try {
            $dealer = Dealer::findOrFail($id);
            $dealer->delete();
            return response()->json(["msj"=>"el dealer fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el dealer que quiere eliminar"]);   
        }
    }
}
