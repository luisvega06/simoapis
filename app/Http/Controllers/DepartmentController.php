<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $department = Department::all();
        return response()->json(["department"=>$department]);
    }

    
    public function store(Request $request)
    {
        $department = Department::create($request->input());
        return response()->json(["department"=>$department]);
    }

    
    public function show($id)
    {
        $department = Department::find($id);
        return response()->json(["department"=>$department]);
    }

    
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        $department->update($request->input());
        return response()->json(["department"=>$department]);
    }


    public function destroy($id)
    {
        try {
            $department = Department::findOrFail($id);
            $department->delete();
            return response()->json(["msj"=>"el departamento fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el departamento que quiere eliminar"]);   
        }
    }
}
