<?php

namespace App\Http\Controllers;

use App\TypeAdviser;
use Illuminate\Http\Request;

class TypeAdviserController extends Controller
{
    public function index()
    {
        $typeAdviser = TypeAdviser::all();
        return response()->json(["typeAdviser"=>$typeAdviser]);
    }

    
    public function store(Request $request)
    {
        $typeAdviser = TypeAdviser::create($request->input());
        return response()->json(["typeAdviser"=>$typeAdviser]);
    }

    
    public function show($id)
    {
        $typeAdviser = TypeAdviser::find($id);
        return response()->json(["typeAdviser"=>$typeAdviser]);
    }

    
    public function update(Request $request, $id)
    {
        $typeAdviser = TypeAdviser::find($id);
        $typeAdviser->update($request->input());
        return response()->json(["typeAdviser"=>$typeAdviser]);
    }


    public function destroy($id)
    {
        try {
            $typeAdviser = TypeAdviser::findOrFail($id);
            $typeAdviser->delete();
            return response()->json(["msj"=>"el tipo de supervisor fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el tipo de supervisor que quiere eliminar"]);   
        }
    }
}
