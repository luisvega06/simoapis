<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidationUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
            return [
                'name' => 'required|max:50', 
                'identification_card' => 'required|max:20|unique:users,identification_card,' . $this->user,
                'email' => 'required|email|max:100|unique:users,email,' . $this->user,
                'password' => 'min:5',
                're_password' => 'required_with:password|min:5|same:password',
                'position_id' => 'required'
            ];
      
    }
}
