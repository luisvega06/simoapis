<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        'name'
    ];

    public function points(){
        return $this->belongsToMany(Point::class);
    }
}
