<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name'
    ];
    public function dealers()
    {
        return $this->hasMany('App\Dealer');
    } 

    public function zones()
    {
        return $this->hasMany('App\Zone');
    }
}
