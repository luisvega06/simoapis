<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

Route::get('/', function () {
    return view('welcome');
});
Route::post('points_excel', 'PointController@importPointsExcel')->name('points_import_excel');
Route::post('points_edit_excel', 'PointController@importPointsEditExcel')->name('points_import_edit_excel');
Route::post('structure_dealer_excel', 'PointController@importStructureDealerEditExcel')->name('structure_dealer_import_excel');

Route::get('cargar_archivos', 'PointController@chargeFile')->name('excel_my_report');


