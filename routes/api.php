<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('users','UserController');
Route::apiResource('positions','PositionController');
Route::apiResource('types','TypeController');
Route::apiResource('departments','DepartmentController');
Route::apiResource('canals','CanalController');
Route::apiResource('dealers','DealerController');
Route::apiResource('zones','ZoneController');
Route::apiResource('frequencies','FrequencyController');
Route::apiResource('circuits','CircuitController');
Route::apiResource('points','PointController');
Route::apiResource('type_advisers','TypeAdviserController');
Route::apiResource('advisers','AdviserController');

Route::post("/auth/login", "AuthController@login");

//Para subir los puntos desde Excel
Route::post('points_excel', 'PointController@importPointsExcel')->name('points_import_excel');
