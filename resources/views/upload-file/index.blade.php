<form action="{{ route('points_import_excel') }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(Session::has('message'))
        <p>{{ Session::get('message') }}</p>
    @endif

    <input type="file" name="filePoint" id="" class="require">
    <button>Importar r point</button>
</form>  

<form action="{{ route('points_import_edit_excel') }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(Session::has('message'))
        <p>{{ Session::get('message') }}</p>
    @endif

    <input type="file" name="filePoint" id="">
    <button>Editar datos del r point</button>
</form> 
<form action="{{ route('structure_dealer_import_excel') }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(Session::has('message'))
        <p>{{ Session::get('message') }}</p>
    @endif

    <input type="file" name="fileStructureDealer" id="">
    <button>Importar estructura de supervisores y asesores dealer</button>
</form> 


