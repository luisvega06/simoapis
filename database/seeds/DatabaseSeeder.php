<?php

use App\Position;
use App\Department;
use App\Canal;
use App\Zone;
use App\Dealer;
use App\Frequency;
use App\TypeAdviser;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        Department::create([
    		"name" => "MAGDALENA"
        ])->save();
        
        Dealer::create([
            "name" => "CONMOVIL",
            "department_id" => "1"
        ])->save();

    	Zone::create([
    		"name" => "SANTA MARTA",
    		"department_id" => "1" 
    	])->save();

    	Position::create([
    		"name" => "SUPERVISOR DE RUTAS",
        ])->save();

      Position::create([
    		"name" => "SUPERVISOR DEALER",
    	])->save();

    	Canal::create([
    		"name" => "DEALER",
        ])->save();

      TypeAdviser::create([
      "name" => "PDA",
      ])->save();
      
      TypeAdviser::create([
      "name" => "PDV",
      ])->save();
        
      Frequency::create([
        "name"     => "Frequencia 1",
        "monday"   => true,
        "tuesday"  => false,
        "wenesday" => true,
        "thursday" => false,
        "friday"   => true,
        "saturday" => false,
        "sunday"   => false
      ]);

      Frequency::create([
        "name"     => "Frequencia 2",
        "monday"   => false,
        "tuesday"  => true,
        "wenesday" => false,
        "thursday" => true,
        "friday"   => false,
        "saturday" => true,
        "sunday"   => false
      ]);

        User::create([
            "name" => "Luis Tigo",
            "identification_card" => "1083005187",
            "password" => bcrypt('secret'),
            "cellphone" => "3017468680",
            "email" => "luisprofee@gmail.com",
            "canal_id" => 1,
            "zone_id" => "1",
            "position_id" =>"1",
            "dealer_id" => "1"

        ])->save();
        
        User::create([
            "name" => "Elkin",
            "identification_card" => "1004376841",
            "password" => bcrypt('1004376841'),
            "cellphone" => "3017468680",
            "email" => "elkin@gmail.com",
            "canal_id" => 1,
            "zone_id" => "1",
            "position_id" =>"2",
            "dealer_id" => "1"

        ])->save();
    }
}
