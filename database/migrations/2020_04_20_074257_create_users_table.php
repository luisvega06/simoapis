<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string("identification_card")->unique()->nullable();
            $table->string("cellphone")->nullable()->nullable();
            $table->string("email")->unique()->nullable();
            $table->string("password")->nullable();
            $table->string("re_password")->nullable()->nullable();

            $table->unsignedBigInteger('canal_id');
            $table->foreign('canal_id')->references('id')->on('canals')
                ->onDelete('cascade');
 
            $table->unsignedBigInteger('zone_id');
            $table->foreign('zone_id')->references('id')->on('zones')
                ->onDelete('cascade');
            
            $table->unsignedBigInteger('position_id');
            $table->foreign('position_id')->references('id')->on('positions')
                ->onDelete('cascade');

            $table->unsignedBigInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
