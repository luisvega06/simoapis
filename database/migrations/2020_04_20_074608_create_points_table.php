<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('channel')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->foreign('type_id')->references('id')->on('types')->nullable();
            $table->text('phone')->nullable();
            $table->text('cell_phone')->nullable();
            $table->string('owner')->nullable();
            $table->string('document_type')->nullable(); 
            $table->string('document')->nullable();
            $table->string('category')->nullable();
            $table->string('schedule')->nullable(); 
            $table->string('department')->nullable();
            $table->string('city')->nullable();
            $table->string('neighborhood')->nullable();
            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('status_dms')->nullable();
            $table->date('creation')->nullable();
            $table->date('last_modification')->nullable();
            $table->string('code_sub')->nullable();
            //Para poder actualizar los cambios de estado de un punto
            $table->string('simcard_service')->nullable();
            $table->string('mbox_service')->nullable();

            $table->unsignedBigInteger('circuit_id')->nullable();
            $table->foreign('circuit_id')->references('id')->on('circuits')
                ->onDelete('cascade');

            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
