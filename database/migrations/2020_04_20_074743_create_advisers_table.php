<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisers', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string("identification_card")->nullable();
            $table->string("cell_phone")->nullable();
            $table->string("password")->nullable();

            $table->unsignedBigInteger('type_adviser_id');
            $table->foreign('type_adviser_id')->references('id')->on('type_advisers')
                ->onDelete('cascade');

            $table->unsignedBigInteger('canal_id');
            $table->foreign('canal_id')->references('id')->on('canals')
                ->onDelete('cascade');
 
            $table->unsignedBigInteger('zone_id');
            $table->foreign('zone_id')->references('id')->on('zones')
                ->onDelete('cascade');
             

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisers');
    }
}
